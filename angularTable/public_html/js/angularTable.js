$(document).ready(function() {
    $(".form-horizontal").hide();
    /*Function to display add company form and displayed table */
    $("#clickAdd").click(function() {
        $("#table").hide();
        $(".form-horizontal").show();
    });
    /*Function to display table after add a new row and hide displayed form */
    $("#submit").click(function() {
         if ($('#company').val() === '') {
            document.getElementById("error-msg").innerHTML = "Enter Company Name";
            document.getElementById("company").focus();
            return false;
        }
        /*          Company Name validation    */
        else
        if ($('#ceo').val() === '') {
            $('#error-msg').html("Enter CEO name");
            document.getElementById("ceo").focus();
            return false;
        }
    /*          Company CEO name validation   */
    else
        if ($('#headquarters').val() === '') {
            $('#error-msg').html("Enter CEO name");
            document.getElementById("headquarters").focus();
            return false;
        }
    /*          Company No. of employee validation    */
        var noe = /^[0-9-@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]*$/;
     if (!noe.test($("#noe").val()) || $("#noe").val() === "" || null) {
            $('#error-msg').html("Enter valid Contact No");
            document.getElementById("noe").focus();
            return false;
        }
         $(".form-horizontal").hide();
        $("#table").show();
         $('#error-msg').html("");
         });
        
});