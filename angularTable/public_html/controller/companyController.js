/*Module used  */
 var helloApp = angular.module("helloApp", []);
helloApp.controller("CompanyCtrl", function($scope) {
    $scope.data=[{company:'Click Lab',ceo:'Samar Singla',headquarters:'California',noe:'200'},
            {company:'TCS', ceo:'Natrajan Chandersekaran', headquarters:'Mumbai', noe:'349600'},
            {company:'Infosys', ceo:'Vishal Sikka', headquarters:'Banglore', noe:'255005'},
            {company:'Wipro', ceo:'Azim Premji', headquarters:'Banglore', noe:'154297'},
            {company:'Accenture', ceo:'pierre Nanterme', headquarters:'Dublin Ireland', noe:'319000'},
            {company:'Tech Mahindra', ceo:'Ramalinga Raju', headquarters:'Pune', noe:'10000'},
            {company:'Mphasis', ceo:'Balu Ganesh Ayyar', headquarters:'Banglore', noe:'45426'},
            {company:'Cisco', ceo:'John Chamber', headquarters:'California', noe:'40000'},
            {company:'Oracle', ceo:'Safra Catz', headquarters:'California', noe:'122458'},
            {company:'SAP', ceo:'Bill McDermott', headquarters:'Germany', noe:'74400'},
            {company:'HCL', ceo:'Anant Gupta', headquarters:'Noida', noe:'100000'},
            {company:'Google', ceo:'Larry Page', headquarters:'California', noe:'53600'},
            {company:'Facebook', ceo:'Mark  Zuckerberg', headquarters:'California', noe:'8348'}];
     /*   dynamically row addition to the table */
        $scope.addRow = function(){		
	$scope.data.push({ 'company':$scope.company, 'ceo': $scope.ceo,
                           'headquarters':$scope.headquarters, 'noe':$scope.noe});
	$scope.company='';
	$scope.ceo='';
	$scope.headquarters='';
        $scope.noe='';
};
});